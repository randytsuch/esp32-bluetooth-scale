# ESP32 Bluetooth Scale

ESP32 interface to a Reflex bluetooth kitchen scale

[Link to Main Project](https://sites.google.com/view/coffee4randy/projects/hx-dumb-to-smart)

[Scale Project link](https://sites.google.com/view/coffee4randy/projects/hx-dumb-to-smart/bluetooth-scale)

Fairly simple program that acquires the weight data from a bluetooth kitchen scale, and sends the data via esp now to another esp32

Copy these files into the same directory to build with arduino program.  First link above, main sites page, has instructions to install all libraries necessary.

- Main program is BluetoothScale_02.ino
- Credentials contains wifi passwords and email info
- ota contains function for OTA programming mode


Weight data is in bytes 5,6 and 7 (1st byte is 0), in bluetooth data packet from scale

MS bit of byte 3 is sign bit.  Byte 3 = 0x05 if positive (or 0x5), and 0x85 if negative number.  Use AND (&) operator to mask bit, then see if set or not
