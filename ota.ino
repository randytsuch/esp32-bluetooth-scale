/*
Webpage and routine OTAWebPage() used for Over The Air program update

called only when button 1 (left one) is pushed during startup
stays in OTA mode forever, until reset without holding button

To create file to use to upload
From Arduino program on top meni go to 
Sketch > Export compiled Binary

this will compile your program, and create a binary file to upload

Go to Sketch > Show Sketch Folder. 
You should have two files in your Sketch folder: the .ino and the .bin file. 
You should upload the .bin file using the OTA Web Updater.
You will need to use "Choose File" button to find and select your .bin file
Select Update, wait for it to upload and reset, it uploads quickly
go to 
192.168.xxx.xxx/update
*/

//**********************Webserver OTA pages **********************
void   OTAWebPage() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(200, "text/plain", "Hi! I am ESP32 BT Scale.");
  });

  AsyncElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");

//run this while loop forever, to upload new program
  while(1) {
   // statement block
   AsyncElegantOTA.loop();
   delay (1);
  }
}
