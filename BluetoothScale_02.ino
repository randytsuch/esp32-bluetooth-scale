/*
 * 
 * Based on ESP32 example, File>Examples>ESP32  BLE Arduino > BLE_Beacon_Scanner
 * 
 * modified to read weight from a reflex bluetooth kitchen scale
 * Added support for elegant OTA.  Ground otaEnPin on power up or reset to go into OTA mode
 * sends weight data to another ESP using esp now protocol
 * 
*/

#include <Arduino.h>

#include <BLEDevice.h>
//#include <BLEUtils.h>
//#include <BLEScan.h>
//#include <BLEAdvertisedDevice.h>

/*
//libs for wifi
#include <esp_wifi.h>
#include <WiFi.h>
//lib for esp now
#include <esp_now.h>   */

//libs for esp now and wifi
#include <esp_now.h>
#include <esp_wifi.h>
#include <WiFi.h>

//lib for ota programming
#include <AsyncElegantOTA.h>

//#include <TelnetStream.h>

// Set your Board ID (ESP32 BluetoothScale = BOARD_ID 1, ESP32 flow = BOARD_ID 2)
#define BOARD_ID 1

#include "Credentials.h" //login info, and other stuff to not make public

//define digital input for OTA mode
//using bottom right pin, next to usb connector
#define otaEnPin 15  //gpio pin to read at startup to decide if OTA mode

// for ota
const char* host = "esp32 BT Scale";
// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create a WebSocket object
AsyncWebSocket ws("/ws");

//#define ENDIAN_CHANGE_U16(x) ((((x)&0xFF00) >> 8) + (((x)&0xFF) << 8))

//Structure to exchange data over ESP Now
//Must match the receiver structure
typedef struct struct_message {
    int Sid;
    double StempGroup;
    double StempBoiler;
    double Srate;       //ml/s from flowmeter
    double StotalML;    //total ml from flowmeter for this shot
    double Spressure;
    double Sweight;    //weight
    int StimerPump;    //not used, used timer from start of flow instead
    int StimerFlow;
    int SreadingId;
} struct_message;


//Create a struct_message called myData
struct_message myData;


int scanTime = 1; //In seconds
BLEScan *pBLEScan;

long digit01;
long digit02;
long digit03;
long weight;
double weight_double;
String temp_string;

  int interval = 500;
  long currentMillis = 0;     // used by interval logic to save current time
  long previousTime = 0;      //used by interval logic to save time of last loop
  long lastUpdateMillis = 0;


/**********************************************************************************************
 * ********************************** . Section of functions . ********************************
 **********************************************************************************************/
 
//*********************  Function to read bluetooth data ******************************************
//

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks
{
    void onResult(BLEAdvertisedDevice advertisedDevice)
    {

        if (advertisedDevice.haveManufacturerData() == true)
        {
          std::string strManufacturerData = advertisedDevice.getManufacturerData();

          uint8_t cManufacturerData[100];
          strManufacturerData.copy((char *)cManufacturerData, strManufacturerData.length(), 0);

     //if statement looks for the weight data packet.  Its always 19 bytes long, and starts with hex ca, 10, 0f     
          if ( (strManufacturerData.length() == 19) && (cManufacturerData[0] == 0xca) && (cManufacturerData[1] == 0x10) && (cManufacturerData[2] == 0xf)) {
            //found a weight data packet, get the 3 hex bytes of weight data
            char tempStr[5];        
            digit01 =  cManufacturerData[7];
            digit02 =  cManufacturerData[6];
            digit03 =  cManufacturerData[5];
            //convert the three digits into the weight value.  Numbers are for hex data
            weight = digit01 + (digit02 * 256) + (digit03 * 65536);

            //MS bit of byte 3 is sign bit.  Byte 3 = 0x05 if positive or 0x85 if negative number.  Use (&) operator to mask bit, then see if set or not
            if ((cManufacturerData[3] & 0x80) == 0x80) {
              weight = -weight;   //if msb of byte 3 is set, number is negative
            }
            //The digit after the decimal was always zero, so added this float conversion and it works now
            float weight_fl = float(weight);
            
            weight_double = weight_fl/100;
            dtostrf(weight_double, 4, 1,  tempStr);    //format weight double float to xx.x char string 
           // Serial.println (String (weight));
            Serial.println (String (tempStr));
   //         Serial2.println (String (tempStr));
            
            myData.Sweight = weight_double;           //store the weight data in the structure to send over esp now
            lastUpdateMillis = millis();              //save time to use to decide if scale is off
          }
        }
        return;
    }
};


//**************************** Function to find wifi channel ***********************************

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}

// *******************  callback when data is sent **********************************
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
//  Serial.print("\r\nLast Packet Send Status:\t");
//  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

/**********************************************************************************************
 * ********************************** . Setup and Main loop . *********************************
 **********************************************************************************************/
 
//*********************************** SETUP ****************************************************

void setup()
{
  Serial.begin(115200);
  
//  Serial2.begin(19200);  //using serial 2 port to send weigh data to Webserver ESP32

   // Define pin
  pinMode (otaEnPin, INPUT_PULLUP);

//************** check for OTA mode, go run OTA routine if pin grounded to update program
if (digitalRead (otaEnPin) == 0) {  //see if OTA button was pushed (left most one)
  OTAWebPage();
}

  Serial.println("Scanning...");

//initialize the bluetooth interface 
  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(200);
  pBLEScan->setWindow(100); // less or equal setInterval value
//  pBLEScan->setInterval(100);
//  pBLEScan->setWindow(99); // less or equal setInterval value


// init wifi interface for esp now
  WiFi.mode(WIFI_STA);

  int32_t channel = getWiFiChannel(ssid);

  WiFi.printDiag(Serial); // Uncomment to verify channel number before
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
  esp_wifi_set_promiscuous(false);
  WiFi.printDiag(Serial); // Uncomment to verify channel change after

  //Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

    // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  //Register peer, which is the webserver ESP
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, peer04, 6);
  peerInfo.encrypt = false;
  
  //Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }


  //initialize variables for board id and update time
  myData.Sid = BOARD_ID;

  lastUpdateMillis = millis();
  
//telnet used for debugging over wifi
//  TelnetStream.begin();

}



//**********************main loop ******************************************************

void loop()
{

  //This section sends weight over esp now once a 1/2 sec (interval)
  
  currentMillis = millis();
  if (currentMillis - previousTime > interval) {
    
    previousTime = millis();

 //check if no data is coming in (scale is turned off)
 //timeout of 100 secs
  if ( (millis() - lastUpdateMillis) > 100000) {
    myData.Sweight = 1.2;          //set to 1.2 to indicate scale is turned off
  }
   
        
 //send data structure over espnow to peer04
    esp_err_t result = esp_now_send(peer04, (uint8_t *) &myData, sizeof(myData));
  }

  //get bluetooth weight data
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
  pBLEScan->clearResults(); // delete results fromBLEScan buffer to release memory

}
